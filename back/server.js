var express = require('express'),
  app = express(),
  port = process.env.PORT || 3001;


//Mongo
//variable para poder usar el request-json
var requestjson = require('request-json');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json

var path = require('path');

var mongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/local";

var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos";
var apiKey = "apiKey=50c5ea68e4b0a97d668bc84a";
var clienteMlab = requestjson.createClient(urlmovimientosMlab + "?" + apiKey);

// INI Configuración Postgre
var pg = require('pg');
// Nombre de la máquina del docker. Máquina puerto y base de datos.
var urlUsuarios = "postgres://docker:docker@localhost:5433/bdseguridad";
var clientePostre = new pg.Client(urlUsuarios); //lo podemos crear aquí o arriba
// voy a asumir que me llega:
// req.body = {usuario:xxxx, password:yyyy}


app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

/*
PRUEBA Inicio con Mongo
*/
app.get('/movimientosmongo', function(req, res) {

  mongoClient.connect(url,function(err,db){
    if(err){
      //alert(1);
      console.log(body);
    }else{
      //alert(2);
      console.log("Connected successfully to server");

    var col = db.collection('movimientos');
    col.find({}).limit(3).toArray(function(err,docs){
    res.send(docs);
    });
      db.close();
    }
  })
});

app.post('/movimientosmongo',function(req, res){
  mongoClient.connect(url,function(err, db){
    var col = db.collection('movimientos');
    console.log("Traying to insert to server");
    /*
    db.collection('movimientos').insertOne({a:1}, function(err,r){
      console.log(r.insertedCount + ' registros insertados');
    });
    db.collection('movimientos').insertMany([{a:2},{a:3}], function(err,r){
      console.log(r.insertedCount + ' registros insertados body');
    });
    */
    db.collection('movimientos').insert(req.body, function(err,r){
       console.log(r.insertedCount + 'registros insertados body');
    });

  db.close();
  res.send("ok");
  })
});
/*
PRUEBA FIN CON MONGO
*/

app.get('/movimientos', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
  clienteMlab = requestjson.createClient(urlmovimientosMlab + "?" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.get('/clientes', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
  clienteMlab = requestjson.createClient(urlmovimientosMlab + "?f={'idcliente':1, 'nombre': 1, 'apellidos': 1}" + "&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.post('/movimientos', function(req, res) {
  /* Crear movimiento en MLab */

});

app.get('/movimientos/:idcliente', function (req, res) {
  /* Obtener movimientos del cliente idcliente desde MLab */
})

// postgre


app.post('/login', function(req, res){
  //crear clientes postgreSQL
  clientPostgre.connect();
  const query = clientPostgre.query('SELECT COUNT(*) FROM usuarios WHERE login=$1 AND password=$2;', [req.body.login, req.body.password], (err, result) => {

    if(err){
      console.log(err);
      res.send(err);
    }
    else{
      if(result.rows[0].count >= 1){
        res.send("Login correcto");
      }else{
        res.send("Login incorrecto");
      }
      /*
      console.log(result.rows[0]);
      res.send(result.rows[0]);*/
    }
  })
  //Hacer consulta
  //Devolver resultado
});
